CC=g++
CFLAGS=-I. -Wall -g -std=c++11


test: test_main.o testesApresentacao.o test_main.o dominiosApresentacao.o Entidades/Apresentacao/Apresentacao.h testesCartao.o dominiosCartao.o Entidades/Cartao/Cartao.h testesEvento.o dominiosEvento.o Entidades/Evento/Evento.h testesIngresso.o dominiosIngresso.o Entidades/Ingresso/Ingresso.h testesUsuario.o dominiosUsuario.o Entidades/Usuario/Usuario.h
	$(CC) $(CFLAGS) -o teste.out testesApresentacao.o test_main.o dominiosApresentacao.o Entidades/Apresentacao/Apresentacao.h testesCartao.o dominiosCartao.o Entidades/Cartao/Cartao.h testesEvento.o dominiosEvento.o Entidades/Evento/Evento.h testesIngresso.o dominiosIngresso.o Entidades/Ingresso/Ingresso.h testesUsuario.o dominiosUsuario.o Entidades/Usuario/Usuario.h

test_main.o: test_main.cpp
	$(CC) $(CFLAGS) -c test_main.cpp

testesApresentacao.o: Entidades/Apresentacao/testesApresentacao.cpp
	$(CC) $(CFLAGS) -c Entidades/Apresentacao/testesApresentacao.cpp

dominiosApresentacao.o: Entidades/Apresentacao/dominiosApresentacao.cpp
	$(CC) $(CFLAGS) -c Entidades/Apresentacao/dominiosApresentacao.cpp

testesCartao.o: Entidades/Cartao/testesCartao.cpp
	$(CC) $(CFLAGS) -c Entidades/Cartao/testesCartao.cpp

dominiosCartao.o: Entidades/Cartao/dominiosCartao.cpp
	$(CC) $(CFLAGS) -c Entidades/Cartao/dominiosCartao.cpp

testesEvento.o: Entidades/Evento/testesEvento.cpp
	$(CC) $(CFLAGS) -c Entidades/Evento/testesEvento.cpp

dominiosEvento.o: Entidades/Evento/dominiosEvento.cpp
	$(CC) $(CFLAGS) -c Entidades/Evento/dominiosEvento.cpp

testesIngresso.o: Entidades/Ingresso/testesIngresso.cpp
	$(CC) $(CFLAGS) -c Entidades/Ingresso/testesIngresso.cpp

dominiosIngresso.o: Entidades/Ingresso/dominiosIngresso.cpp
	$(CC) $(CFLAGS) -c Entidades/Ingresso/dominiosIngresso.cpp

testesUsuario.o : Entidades/Usuario/testesUsuario.cpp
	$(CC) $(CFLAGS) -c Entidades/Usuario/testesUsuario.cpp

dominiosUsuario.o: Entidades/Usuario/dominiosUsuario.cpp
	$(CC) $(CFLAGS) -c Entidades/Usuario/dominiosUsuario.cpp

clean:
	rm -f *.o
	echo Clean done
