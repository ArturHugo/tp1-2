#ifndef IA_CLIENTE_H
#define IA_CLIENTE_H

#include "Usuario.h"
#include "dominiosUsuario.h"
#include "Cartao.h"
#include "dominiosCartao.h"
#include "ISCliente.h"
#include "Resultados.h"

class IACliente
{
public:
    // Método para criar cadastro no sistema.
    virtual ResultadoCadastro efetuarCadastro() = 0;

    // Método para conectar com a camada de serviço.
    virtual void setCSCliente(ISCliente *) = 0;

    // Método destrutor virtual.
    virtual ~IACliente(){}
};

#endif