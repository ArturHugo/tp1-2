#ifndef IS_CLIENTE_H
#define IS_CLIENTE_H

#include "Usuario.h"
#include "dominiosUsuario.h"
#include "Cartao.h"
#include "Resultados.h"

class ISCliente
{
public:
    // Método por meio do qual é solicitado o serviço.
    virtual Resultado efetuarCadastro(const CPF&, const Senha&) = 0;

    // Método destrutor virtual.
    virtual ~ISCliente(){}
};

#endif