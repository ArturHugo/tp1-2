#ifndef DOM_INGRESSO_H
#define DOM_INGRESSO_H

#include <stdexcept>

using namespace std;

///
/// %Classe de domínio para código de ingresso.
///
/// A classe consiste em uma string de cinco caracteres
/// numéricos. A classe inclui métodos setter e getter
/// para o valor da string do código.
///
class CodigoIngresso{
private:

    const static int TAMANHO = 5;

    string codigo;
    
    // Método de validação
    void validar(string);

public:

    void setCodigo(string); ///< Método setter para atribuir valor ao código.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação.
                    
    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    string getCodigo() const{
        return codigo;
    }
};


#endif