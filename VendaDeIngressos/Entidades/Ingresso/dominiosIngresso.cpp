#include "dominiosIngresso.h"

using namespace std;

//Métodos do domínio Código;

void CodigoIngresso::validar(string codigo){
    // Verificando se o tamanho do Código está correto
    if(codigo.length() != TAMANHO)
        throw invalid_argument("Código de ingresso inválido: tamanho errado.");
    else{
        for(auto i = 0; i < int(codigo.length()); i++){
            // Verificando se o Código contém os caracteres válidos
            if(codigo[i] < '0' || codigo[i] > '9')
                throw invalid_argument("Código de ingresso inválido: caracteres não numéricos.");
        }
    }
}

void CodigoIngresso::setCodigo(string codigo){
    validar(codigo);
    this->codigo = codigo;
}