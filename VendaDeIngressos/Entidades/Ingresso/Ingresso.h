#ifndef ENTIDADE_INGRESSO_H
#define ENTIDADE_INGRESSO_H

#include "dominiosIngresso.h"

using namespace std;

///
/// %Classe da entidade ingresso.
///
class Ingresso{
private:

    CodigoIngresso codigo;

public:

    ///
    /// Método setter para o código de ingresso.
    ///
    void setCodigo(CodigoIngresso &codigo) {
        this->codigo = codigo;
    }

    ///
    /// Método getter para o código de evento.
    ///
    CodigoIngresso getCodigo() const {
        return codigo;
    }
};
#endif