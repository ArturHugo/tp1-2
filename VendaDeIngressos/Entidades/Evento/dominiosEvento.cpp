#include "dominiosEvento.h"

using namespace std;

//Metodos do dominio Código;

void CodigoEvento::validar(string codigo){
    // Verificando se o tamanho do Código está correto
    if(codigo.length() != TAMANHO)
        throw invalid_argument("Código de evento inválido: tamanho errado.");
    else{
        for(auto i = 0; i < int(codigo.length()); i++){
            // Verificando se o Código contém os caracteres válidos
            if(codigo[i] < '0' || codigo[i] > '9')
                throw invalid_argument("Código de evento inválido: caracteres não numéricos.");
        }
    }
}

void CodigoEvento::setCodigo(string codigo){
    validar(codigo);
    this->codigo = codigo;
}

bool NomeEvento::verificaLetra(char caracter){
    if((caracter >= 'a' && caracter <= 'z') || (caracter >= 'A' && caracter <= 'Z'))
        return 1;
    else
        return 0;
}

void NomeEvento::validar(string nome){
    bool tem_letra = 0;
    // Verificando se o tamanho do nome está correto
    if(nome.length() > TAMANHO)
        throw invalid_argument("Nome inválido: muito grande.");
    else{
        //Verificando se o primeiro caracter é válido
        if((nome[0] != ' ') && (nome[0] != '-')){
            if(verificaLetra(nome[0]))
                tem_letra = 1;
            else
                throw invalid_argument("Nome inválido: caracteres inválidos.");
        }

        //Verificando o resto da string
        for(auto i = 1; i < int(nome.length()); i++){
            // Verificando se há espaços em sequência
            if(nome[i] == ' ' && nome[i-1] == ' ')
                throw invalid_argument("Nome inválido: espaços em sequência.");
            // Verificando se o nome contém os caracteres válidos  
            if((nome[i] != ' ') && (nome[i] != '-')){
                if(verificaLetra(nome[i]))
                    tem_letra = 1;
                else
                    throw invalid_argument("Nome inválido: caracteres inválidos.");
            }
        }
    }
    // Verificando se pelo menos um dos caracteres é uma letra
    if(!tem_letra)
        throw invalid_argument("Nome inválido: não contém letras.");
}

void NomeEvento::setNome(string nome){
    validar(nome);
    this->nome = nome;
}

bool Cidade::verificaLetra(char caracter){
    if((caracter >= 'a' && caracter <= 'z') || (caracter >= 'A' && caracter <= 'Z'))
        return 1;
    else
        return 0;
}

void Cidade::validar(string cidade){
    bool tem_letra = 0;
    //Verificando se o tamanho do nome da cidade está correto
    if(cidade.length() > TAMANHO)
        throw invalid_argument("Cidade inválida: nome muito grande.");
    else{
        //Verificando se o primeiro caracter é válido
        if((cidade[0] != ' ') && (cidade[0] != '.')){
            if(verificaLetra(cidade[0])) 
                tem_letra = 1;
            else
                throw invalid_argument("Cidade inválida: caracteres inválidos.");
        }
        //Verificando o resto da string
        for(auto i = 1; i < int(cidade.length()); i++){
            // Verificando se há espaços em sequência
            if(cidade[i] == ' ' && cidade[i-1] == ' ')
                throw invalid_argument("Cidade inválida: espaços em sequência.");
            // Verificando se ponto é precedido por letra
            if(cidade[i] == '.' && !(verificaLetra(cidade[i-1])))
                throw invalid_argument("Cidade inválida: ponto não é precedido por letra.");
            // Verificando se o nome da cidade contém os caracteres válidos  
            if((cidade[i] != ' ') && (cidade[i] != '.')){
                if(verificaLetra(cidade[i])) 
                    tem_letra = 1;
                else
                    throw invalid_argument("Cidade inválida: caracteres inválidos.");
            }
        }
    }
    if(!tem_letra)
        throw invalid_argument("Cidade inválida: sem letra.");
}

void Cidade::setCidade(string cidade){
    validar(cidade);
    this->cidade = cidade;
}

void Estado::validar(string estado){    
    //Verificando se a sigla corresponde a um dos estados existentes
    if(!(estado == "AC" || estado == "AL" || estado == "AP" || estado == "AM" ||
         estado == "BA" || estado == "CE" || estado == "DF" || estado == "ES" || 
         estado == "GO" || estado == "MA" || estado == "MT" || estado == "MS" || 
         estado == "MG" || estado == "PA" || estado == "PB" || estado == "PR" || 
         estado == "PE" || estado == "PI" || estado == "RJ" || estado == "RN" || 
         estado == "RS" || estado == "RO" || estado == "RR" || estado == "SC" || 
         estado == "SP" || estado == "SE" || estado == "TO" ))
        throw invalid_argument("Estado inválido.");  

}

void Estado::setEstado(string estado){
    validar(estado);
    this->estado = estado;
}

void Classe::validar(int classe){
    //Verificando se o número da classe é válido
    if(classe < MIN || classe > MAX)
        throw invalid_argument("Classe inválida.");
}

void Classe::setClasse(int classe){
    validar(classe);
    this->classe = classe;
    switch(classe){
        case 1:
            this->nome_classe = "TEATRO";
            break;
        case 2:
            this->nome_classe = "ESPORTE";
            break;
        case 3:
            this->nome_classe = "SHOW NACIONAL";
            break;
        case 4:
            this->nome_classe = "SHOW INTERNACIONAL";
            break;
    }
}

void Faixa::validar(string faixa){
    //Verificando se o valor corresponde a uma faixa etária válida
    if(!(faixa == "L"  || faixa == "10" || faixa == "12"|| 
         faixa == "14" || faixa == "16" || faixa == "18"))
        throw invalid_argument("Faixa etária inválida");
}

void Faixa::setFaixa(string faixa){
    validar(faixa);
    this->faixa = faixa;
}
