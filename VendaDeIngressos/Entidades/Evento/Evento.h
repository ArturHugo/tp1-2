#ifndef ENTIDADE_EVENTO_H
#define ENTIDADE_EVENTO_H

#include "dominiosEvento.h"

using namespace std;


///
/// %Classe da entidade evento.
///
class Evento{
private:
    CodigoEvento codigo;
    NomeEvento nome;
    Cidade cidade;
    Estado estado;
    Classe classe;
    Faixa faixa;
public:

    ///
    /// Método setter para o código de evento.
    ///
    void setCodigo(const CodigoEvento &codigo) {
        this->codigo = codigo;
    }

    ///
    /// Método getter para o código de evento.
    ///
    CodigoEvento getCodigo() const {
        return codigo;
    }

    ///
    /// Método setter para o nome de evento.
    ///
    void setNome(NomeEvento &nome) {
        this->nome = nome;
    }

    ///
    /// Método getter para o nome de evento.
    ///
    NomeEvento getNome() const {
        return nome;
    }

    ///
    /// Método setter para a cidade.
    ///
    void setCidade(Cidade &cidade) {
        this->cidade = cidade;
    }

    ///
    /// Método getter para a cidade.
    ///
    Cidade getCidade() const {
        return cidade;
    }

    ///
    /// Método setter para o estado.
    ///
    void setEstado(Estado &estado) {
        this->estado = estado;
    }

    ///
    /// Método getter para o estado.
    ///
    Estado getEstado() const {
        return estado;
    }

    ///
    /// Método setter para a classe.
    ///
    void setClasse(Classe &classe) {
        this->classe = classe;
    }

    ///
    /// Método getter para a classe.
    ///
    Classe getClasse() const {
        return classe;
    }

    ///
    /// Método setter para a faixa etária.
    ///
    void setFaixa(Faixa &faixa) {
        this->faixa = faixa;
    }

    ///
    /// Método getter para a faixa etária.
    ///
    Faixa getFaixa() const {
        return faixa;
    }
};

#endif