#ifndef DOM_EVENTO_H
#define DOM_EVENTO_H

#include <stdexcept>

using namespace std;

///
/// %Classe de domínio para código de evento.
///
/// A classe consiste em uma string de três caracteres
/// numéricos. A classe inclui métodos setter e getter
/// para o valor da string do código.
///
class CodigoEvento{
private:

    const static int TAMANHO = 3;

    string codigo;
    
    // Método de validação
    void validar(string);

public:

    void setCodigo(string); ///< Método setter para atribuir valor ao código.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    string getCodigo() const{
        return codigo;
    }
};

///
/// %Classe de domínio para nome de evento.
///
/// A classe consiste em uma string de vinte caracteres. 
/// É delimitado que os caracteres sejam apenas letras,
/// espaços ou dígitos. Não são permitidos nomes onde
/// dois caracteres repetidos são de espaço. A classe inclui
/// métodos setter e getter para o valor da string do nome.
///
class NomeEvento{
private:
    const static int TAMANHO = 20;

    string nome;

    // Método que verifica se um caracter é letra (maiúscula ou minúscula)
    bool verificaLetra(char);
    
    // Método de validação
    void validar(string);

public:

    void setNome(string);   ///< Método setter para atribuir valor ao nome.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///    
    string getNome() const{
        return nome;
    }
};

///
/// %Classe de domínio para cidade.
///
/// A classe consiste em uma string de quinze caracteres. 
/// É delimitado que os caracteres sejam apenas letras,
/// espaços ou pontos. Não são permitidos nomes onde
/// dois caracteres repetidos são de espaço. Também, para
/// cada caracter de ponto, há um caracter de letra que 
/// o precede. A classe inclui métodos setter e getter para
/// o valor da string do nome.
///
class Cidade{
private:
    const static int TAMANHO = 15;

    string cidade;

    // Método que verifica se um caracter é letra (maiúscula ou minúscula)
    bool verificaLetra(char);

    // Método de validação
    void validar(string);

public:

    void setCidade(string); ///< Método setter para atribuir valor à cidade.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    /// 
    string getCidade() const{
        return cidade;
    }
};

///
/// %Classe de domínio para estado.
///
/// A classe consiste em uma string de dois caracteres, 
/// os quais representam a sigla de um dos estados do Brasil.
/// A classe inclui métodos setter e getter para o valor da
/// string do nome.
///

class Estado{
private:
    string estado;

    // Método de validação
    void validar(string);

public:

    void setEstado(string); ///< Método setter para atribuir valor ao estado.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    /// 
    string getEstado() const{
        return estado;
    }
};

///
/// %Classe de domínio para classe.
///
/// A classe consiste em um valor inteiro de 1 a 4, e uma 
/// string que representa o nome da classe. Para cada valor
/// da classe há um nome associado, definido no método getter. 
/// A classe inclui métodos setter e getter para o valor da
/// string do nome.
///
class Classe{
private:
    const static int MIN = 1;
    const static int MAX = 4;

    int classe;
    string nome_classe;

    // Método de validação
    void validar(int);

public:

    void setClasse(int);    ///< Método setter para atribuir valor à classe.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação. Além disso,
                            ///< ele atribui um valor ao nome que depende do valor
                            ///< da classe.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    /// 
    int getClasse() const{
        return classe;
    }
};

///
/// %Classe de domínio para faixa etária.
///
/// A classe consiste em uma string que representa a faixa
/// etária do evento. Ela inclui métodos setter e getter para
/// o valor da string.
///
class Faixa{
private:
    string faixa;

    // Método de validação
    void validar(string);

public:

    void setFaixa(string);  ///< Método setter para atribuir valor à faixa etária.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação. Os valores desse
                            ///< domínio são os seguintes: "L", "10", "12", "14", "16" e "18".

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    /// 
    string getFaixa(){
        return faixa;
    }
};



#endif