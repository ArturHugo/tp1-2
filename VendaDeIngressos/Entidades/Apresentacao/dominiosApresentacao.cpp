#include "dominiosApresentacao.h"

using namespace std;


// Métodos do domínio Código de apresentação.
void CodigoApresentacao::validar(string codigo) {
    if(codigo.length() != TAMANHO)
        throw invalid_argument("Código de apresentação inválido: tamanho errado.\n");
    else
        for(auto i = 0; i < TAMANHO; i++)
            if(codigo[i] < CODIGO_CHAR_MIN || codigo[i] > CODIGO_CHAR_MAX)
                throw invalid_argument("Código de apresentação inválido: caracteres não numéricos.\n");
}

void CodigoApresentacao::setCodigo(string codigo) {
    validar(codigo);
    this->codigo = codigo;
}

// Métodos do domínio Data.
void Data::validar(TipoData data) {
    bool ano_bissexto;
    int dias_no_mes;
    int dia, mes, ano;
    tie(dia, mes, ano) = data;

    // Validando ano.
    if(ano < 0 || ano > LIMITE_ANO)
        throw invalid_argument("Ano inválido.\n");

    // Validando mes.
    if(mes < 1 || mes > LIMITE_MES)
        throw invalid_argument("Mês inválido.\n");
    
    // Verificando ano bissexto.
    ano_bissexto = (ano % 4 == 0);

    // Verificando quantidade de dias no mês.
    if(mes == 2)
        if(ano_bissexto)
            dias_no_mes = 29;
        else
            dias_no_mes = 28;
    else
        if(mes % 2 != 0)
            if(mes > 8)
                dias_no_mes = 30;
            else
                dias_no_mes = 31;
        else
            if(mes >= 8)
                dias_no_mes = 31;
            else
                dias_no_mes = 30;

    // Validando dia.
    if(dia < 1 || dia > dias_no_mes)
        throw invalid_argument("Dia inválido.\n");
}

void Data::setData(TipoData data) {
    validar(data);
    this->data = data;
}

// Métodos do domínio Horário.
void Horario::validar(TipoHorario horario) {
    int hora, minuto;
    tie(hora, minuto) = horario;
    if(minuto % MULTIPLO_MINUTO != 0){
        throw invalid_argument("Minuto inválido.\n");
    } else if(minuto < MIN_MINUTO || minuto > MAX_MINUTO){
        throw invalid_argument("Minuto inválido.\n");
    }
    if(hora < MIN_HORA || hora > MAX_HORA){
        throw invalid_argument("Hora inválido.\n");
    }
}

void Horario::setHorario(TipoHorario horario) {
    validar(horario);
    this->horario = horario;
}

// Métodos do domínio Preço.
void Preco::validar(float preco) {
    if(preco < MINIMO || preco > MAXIMO){
        throw invalid_argument("Preço inválido.\n");
    }
}

void Preco::setPreco(float preco) {
    validar(preco);
    this->preco = preco;
}

// Métodos do domínio Número de sala.
void NumeroSala::validar(int numero) {
    if(numero < MINIMO || numero > MAXIMO){
        throw invalid_argument("Número de sala inválido.\n");
    }
}

void NumeroSala::setNumero(int numero) {
    validar(numero);
    this->numero = numero;
}

// Métodos do domínio Disponibilidade.
void Disponibilidade::validar(int disponibilidade) {
    if(disponibilidade < 0 || disponibilidade > LIMITE){
        throw invalid_argument("Número de disponibilidade inválido.\n");
    }
}

void Disponibilidade::setDisponibilidade(int disponibilidade) {
    validar(disponibilidade);
    this->disponibilidade = disponibilidade;
}