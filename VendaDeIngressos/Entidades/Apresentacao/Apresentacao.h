#ifndef ENTIDADE_APRESENTACAO_H
#define ENTIDADE_APRESENTACAO_H

#include "dominiosApresentacao.h"

using namespace std;

///
/// %Classe da entidade apresentação.
///
class Apresentacao {
private:

    CodigoApresentacao codigo;
    Data data;
    Horario horario;
    Preco preco;
    NumeroSala sala;
    Disponibilidade disponibilidade;

public:

    ///
    /// Método setter para o código da apresentação.
    ///
    void setCodigo(const CodigoApresentacao &codigo) {
        this->codigo = codigo;
    }

    ///
    /// Método getter para o código da apresentação.
    ///
    CodigoApresentacao getCodigo() const {
        return codigo;
    }

    ///
    /// Método setter para a data da apresentação.
    ///
    void setData(const Data &data) {
        this->data = data;
    }

    ///
    /// Método getter para a data da apresentação.
    ///
    Data getData() const {
        return data;
    }

    ///
    /// Método setter para o horário da apresentação.
    ///
    void setHorario(const Horario &horario) {
        this->horario = horario;
    }

    ///
    /// Método getter para o horário da apresentação.
    ///
    Horario getHorario() const {
        return horario;
    }

    ///
    /// Método setter para o preço da apfesentação.
    ///
    void setPreco(const Preco &preco) {
        this->preco = preco;
    }

    ///
    /// Método getter para o preço da apresentação.
    ///
    Preco getPreco() const {
        return preco;
    }

    ///
    /// Método setter para o número da sala da apresentação.
    ///
    void setSala(const NumeroSala &sala) {
        this->sala = sala;
    }

    ///
    /// Método getter para o número da sala da apresentação.
    ///
    NumeroSala getSala() const {
        return sala;
    }

    ///
    /// Método setter para a disponibilidade de ingressos da apresentação.
    ///
    void setDisponibilidade(const Disponibilidade &disponibilidade) {
        this->disponibilidade = disponibilidade;
    }

    ///
    /// Método getter para a disponibilidade de ingressos da apresentação.
    ///
    Disponibilidade getDisponibilidade() const {
        return disponibilidade;
    }

};


#endif