#ifndef DOM_APRESENTACAO_H
#define DOM_APRESENTACAO_H

#include <tuple>
#include <stdexcept>

#define TipoData tuple <int, int, int>
#define TipoHorario tuple <int, int>

using namespace std;

///
/// %Classe de domínio para código de apresentação.
///
/// O código de apresentação consiste em uma string
/// de quatro caracteres numéricos. A classe inclui
/// métodos setter e getter para o valor da string do código.
///
class CodigoApresentacao {
private:

    string codigo;

    const static int TAMANHO = 4;
    const static int CODIGO_CHAR_MAX = 57;
    const static int CODIGO_CHAR_MIN = 48;

    // Método de validação.
    void validar(string);

public:

    void setCodigo(string); ///< Método setter para atribuir valor ao código.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    string getCodigo() const {
        return codigo;
    }
};


///
/// %Classe de domínio para data.
///
/// A estrutura utilizada foi o TipoData,
/// definido como uma tupla de três inteiros.
/// Cada elemento de uma estrutura TipoData pode ser
/// acessado de maneira mais intuitiva usando-se as constantes
/// DIA, MES e ANO, por exemplo: ObjetoData.getData().get(DIA).
///
class Data {
private:

    TipoData data;

    const static int DIA = 0;
    const static int MES = 1;
    const static int ANO = 2;

    const static int LIMITE_MES = 12;
    const static int  LIMITE_ANO = 99;

    // Mẽtodo de validação.
    void validar(TipoData);

public:

    void setData(TipoData); ///< Método setter para atribuir valor à data.
                            ///< O método recebe um valor de TipoData e o passa
                            ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    TipoData getData() const {
        return data;
    }
};


///
/// %Classe de domínio para horário.
///
/// A estrutura utilizada foi o TipoHorario,
/// definido como uma tupla de dois inteiros.
/// Cada elemento de uma estrutura TipoHorario pode ser
/// acessado de maneira mais intuitiva usando-se as constantes
/// MINUTO e HORA, por exemplo: ObjetoHorario.getHorario().get(MINUTO).
///
class Horario {
private:

    TipoHorario horario;

    const static int MINUTO = 0;
    const static int HORA = 1;

    const static int MIN_HORA = 7;
    const static int MAX_HORA = 22;
    const static int MIN_MINUTO = 0;
    const static int MAX_MINUTO = 45;
    const static int MULTIPLO_MINUTO = 15;

    // Método de validação.
    void validar(TipoHorario);

public:

    void setHorario(TipoHorario);   ///< Método setter para atribuir valor ao horário.
                                    ///< O método recebe um valor de TipoHorario e o passa
                                    ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    TipoHorario getHorario() const {
        return horario;
    }
};


///
/// %Classe de domínio para preço.
///
/// A classe consiste de um valor float para o preço e
/// respectivos métodos setter e getter. O valores mínimos
/// e máximos para um preço foram definidos como 0 e 1000 respectivamente.
///
class Preco {
private:

    float preco;

    constexpr static float MINIMO = 0;
    constexpr static float MAXIMO = 1000;

    // Método de validação.
    void validar(float);

public:

    void setPreco(float);   ///< Método setter para atribuir valor ao preço.
                            ///< O método recebe um valor de float e o passa
                            ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    float getPreco() const {
        return preco;
    }
};


///
/// %Classe de domínio para número de sala.
///
/// A classe consiste de um valor inteiro para o número de sala e
/// respectivos métodos setter e getter. O valores mínimos
/// e máximos para um número de sala foram definidos como 1 e 10 respectivamente.
///
class NumeroSala {
private:

    int numero;

    const static int MINIMO = 1;
    const static int MAXIMO = 10;

    // Método de validação.
    void validar(int);

public:

    void setNumero(int);    ///< Método setter para atribuir valor ao número de sala.
                            ///< O método recebe um valor do tipo int e o passa
                            ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    int getNumero() const {
        return numero;
    }
};


///
/// %Classe de domínio para disponibilidade.
///
/// A classe consiste de um valor inteiro para inidicar o número de ingressos disponíveis
/// e respectivos métodos setter e getter. O valores mínimos
/// e máximos para número de ingressos disponíveis foram definidos como 0 e 250 respectivamente.
///
class Disponibilidade {
private:

    int disponibilidade;

    const static int LIMITE = 250;

    // Método de validação.
    void validar(int);

public:

    void setDisponibilidade(int);   ///< Método setter para atribuir valor à disponibilidade.
                                    ///< O método recebe um valor do tipo int e o passa
                                    ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    int getDisponibilidade() const {
        return disponibilidade;
    }
};

#endif