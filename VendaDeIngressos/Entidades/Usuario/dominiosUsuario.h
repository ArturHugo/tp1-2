#ifndef DOM_USUARIO_H
#define DOM_USUARIO_H

#include <stdexcept>

using namespace std;

///
/// %Classe de domínio para CPF
///
/// A classe consiste em uma string de onze caracteres
/// numéricos. O CPF precisa passar pelo algoritmo dem Luhn.
/// A classe inclui métodos setter e getter para o 
/// valor da string do código.
///
class CPF{
private:
    string cpf;

    const static int TAMANHO = 11;

    // Método de validacao.
    void validar(string);   
public:

    void setCPF(string);    ///< Método setter para atribuir valor ao CPF.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação. No processo de
                            ///< validação desse domínio, é utilizado o algoritmo de Luhn.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    string getCPF() const{
        return cpf;
    }

};

///
/// %Classe de domínio para a senha.
///
/// A classe consiste em uma string de 6 caracteres
/// numéricos. Ela precisa conter pelo menos uma letra,
/// uma letra maiúscula, uma letra minúscula, e um número.
/// Os caracteres não podem ser dieferentes desses citados.
/// A classe inclui métodos setter e getter para o 
/// valor da string do código.
///
class Senha{
private:
    string senha;

    const static int TAMANHO = 6;

    // Metodo de validacao.
    void validar(string);

public:

    void setSenha(string);  ///< Método setter para atribuir valor à senha.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    string getSenha() const{
        return senha;
    }
};

#endif