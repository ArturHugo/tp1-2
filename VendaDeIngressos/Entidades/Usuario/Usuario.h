#ifndef ENTIDADE_USUARIO_H
#define ENTIDADE_USUARIO_H

#include "dominiosUsuario.h"

using namespace std;


///
/// %Classe da entidade usuário.
///
class Usuario{
private:

    CPF cpf;
    Senha senha;

public:

    ///
    /// Método setter para o cpf.
    ///
    void setCPF(CPF &cpf) {
        this->cpf = cpf;
    }

    ///
    /// Método getter para o cpf.
    ///
    CPF getCPF() const {
        return cpf;
    }

    ///
    /// Método setter para a senha.
    ///
    void setSenha(Senha &senha) {
        this->senha = senha;
    }

    ///
    /// Método getter para a senha.
    ///
    Senha getSenha() const {
        return senha;
    }
};

#endif