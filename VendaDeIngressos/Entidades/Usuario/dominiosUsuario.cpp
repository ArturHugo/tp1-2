#include "dominiosUsuario.h"

using namespace std;

void CPF::validar(string cpf) {
    // Verificando se o tamanho do CPF está correto.
    if(cpf.length() != TAMANHO)
        throw invalid_argument("CPF inválido: tamanho errado.");
    else{
        int soma_luhn = 0;
        int digito;

        // Verificando se dígitos são estritamente numéricos e se
        // o CPF passa no algoritmo de Luhn.
        for(auto i = 0; i < TAMANHO; i++){
            if(cpf[i] < '0' || cpf[i] > '9')
                throw invalid_argument("CPF inválido: caracteres não numéricos.");

            digito = cpf[i] - '0';

            // Somando dígitos de acordo com o algoritmo de Luhn.
            if((TAMANHO-i)%2 == 0){
                digito *= 2;
                if(digito > 9)
                    digito -= 9;
            }
            soma_luhn += digito;
        }
        if(soma_luhn % 10 != 0)
            throw invalid_argument("CPF inválido: não passa no algoritmo de Luhn.\n");
    }
};

void CPF::setCPF(string cpf){
    validar(cpf);
    this->cpf = cpf;
};

void Senha::validar(string senha){
    bool minusculo, maiusculo, numero;
    minusculo = 0;
    maiusculo = 0;
    numero    = 0;
    // Verificando se o tamanho da senha está correto
    if(senha.length() != TAMANHO)
        throw invalid_argument("Senha inválida: tamanho errado.");
    else{
        for(auto i = 0; i < int(senha.length()); i++){
            // Verificando se a senha contém os caracteres válidos
            if((senha[i] >= 'a' && senha[i] <= 'z') || (senha[i] >= 'A' && senha[i] <= 'Z') || (senha[i] >= '0' && senha[i] <= '9')){
                //Verificando se a senha contém pelo menos um de cada tipo de caracter
                if(senha[i] >= 'a' && senha[i] <= 'z')
                    minusculo = 1;
                if(senha[i] >= 'A' && senha[i] <= 'Z')
                    maiusculo = 1;
                if(senha[i] >= '0' && senha[i] <= '9')
                    numero = 1;        
            }
            else
                throw invalid_argument("Senha inválida: caracter inválido encontrado.");
        }
        if(!minusculo)
            throw invalid_argument("Senha inválida: nenhum caracter minúsculo.");
        if(!maiusculo)
            throw invalid_argument("Senha inválida: nenhum caracter maiúsculo.");
        if(!numero)
            throw invalid_argument("Senha inválida: nenhum caracter numérico.");
    }
};

void Senha::setSenha(string senha){
    validar(senha);
    this->senha = senha;
};

