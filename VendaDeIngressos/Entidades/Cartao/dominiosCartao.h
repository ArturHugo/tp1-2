#ifndef DOM_CARTAO_H
#define DOM_CARTAO_H

#include <tuple>
#include <stdexcept>

#define TipoValidade tuple <int, int>

using namespace std;


///
/// %Classe de domínio para cartão de crédito.
///
/// A classe consiste em uma string de 16 caracteres numéricos
/// para representar um número de cartão de créditoe inclui seus métodos
/// setter e getter.
///
class NumeroCartao {
private:

    string numero;

    const static int TAMANHO = 16;
    const static int NUMERO_CHAR_MAX = 57;
    const static int NUMERO_CHAR_MIN = 48;

    // Método de validação.
    void validar(string);

public:

    void setNumero(string); ///< Método setter para atribuir valor ao número do cartão.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    string getNumero() const {
        return numero;
    }

};


///
/// %Classe de domínio para código de seguraça.
///
/// A classe consiste em uma string de 3 caracteres numéricos
/// para representar o código de egurança de um cartão de crédito e inclui
/// seus métodos setter e getter.
///
class CodigoSeguranca {
private:

    string codigo;

    const static int TAMANHO = 3;
    const static int CODIGO_CHAR_MAX = 57;
    const static int CODIGO_CHAR_MIN = 48;
    
    // Método de validação.
    void validar(string);

public:

    void setCodigo(string); ///< Método setter para atribuir valor ao código de segurança.
                            ///< O método recebe um valor de string e o passa
                            ///< pelo respectivo processo de validação. No processo de
                            ///< validação desse domínio, é utilizado o algoritmo de Luhn.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    string getCodigo() const {
        return codigo;
    }

};


///
/// %Classe de domínio para data de validade.
///
/// A classe consiste em um valor TipoValidade, definido como
/// uma tupla de dois números inteiros que representam respectivamente
/// mês e ano de uma validade. Para acessar os valores de um TipoValidade
/// de forma mais intuitiva, utilize as constantes MES e ANO, por exemplo:
/// ObjetoValidade.getValidade().get(MES).
/// A classe inclui métodos setter e getter para o valor mencionado.
///
class DataValidade {
private:

    TipoValidade data;

    const static int MES = 0;
    const static int ANO = 1;
    const static int MES_MAX = 12;
    const static int MES_MIN = 1;
    const static int ANO_MAX = 99;
    const static int ANO_MIN = 0;
    
    // Método de validação.
    void validar(TipoValidade);

public:

    void setData(TipoValidade); ///< Método setter para atribuir valor à data de validade.
                                ///< O método recebe um valor de TipoValidade e o passa
                                ///< pelo respectivo processo de validação.

    ///
    /// Método getter para retornar o valor de um objeto dessa classe.
    ///
    TipoValidade getData() const {
        return data;
    }

};

#endif