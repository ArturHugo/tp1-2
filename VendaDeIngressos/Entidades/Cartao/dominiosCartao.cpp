#include "dominiosCartao.h"

using namespace std;


// Métodos do domínio Número de cartão.
void NumeroCartao::validar(string numero) {
    if(numero.length() != TAMANHO)
        throw invalid_argument("Número de cartão inválido: tamanho errado.\n");
    else {
        int soma_luhn = 0;
        int digito;
        for(auto i = 0; i < TAMANHO; i++){
            if(numero[i] < NUMERO_CHAR_MIN || numero[i] > NUMERO_CHAR_MAX)
                throw invalid_argument("Número de cartão inválido: caracteres não numéricos.\n");

            digito = numero[i] - NUMERO_CHAR_MIN;

            // Somando dígitos de acordo com o algoritmo de Luhn.
            if((TAMANHO-i)%2 == 0){
                digito *= 2;
                if(digito > 9)
                    digito -= 9;
            }
            soma_luhn += digito;
        }
        if(soma_luhn % 10 != 0)
            throw invalid_argument("Número de cartão inválido: não passa no algoritmo de Luhn.\n");
    }
}

void NumeroCartao::setNumero(string numero) {
    validar(numero);
    this->numero = numero;
}

// Métodos do domínio Código de segurança.
void CodigoSeguranca::validar(string codigo) {
    if(codigo.length() != TAMANHO)
        throw invalid_argument("Código de segurança inválido: tamanho errado.\n");
    else
        for(auto i = 0; i < TAMANHO; i++)
            if(codigo[i] < CODIGO_CHAR_MIN || codigo[i] > CODIGO_CHAR_MAX)
                throw invalid_argument("Código de segurança inválido: caracteres não numéricos.\n");
}

void CodigoSeguranca::setCodigo(string codigo) {
    validar(codigo);
    this->codigo = codigo;
}

// Métodos do domínio Data de validade.
void DataValidade::validar(TipoValidade data) {
    int mes, ano;
    tie(mes, ano) = data;

    if(mes < MES_MIN || mes > MES_MAX)
        throw invalid_argument("Data de validade inválida: mês inválido.\n");
    else if(ano < ANO_MIN || ano > ANO_MAX)
        throw invalid_argument("Data de validade inválida: ano inválido.\n");
}

void DataValidade::setData(TipoValidade data) {
    validar(data);
    this->data = data;
}