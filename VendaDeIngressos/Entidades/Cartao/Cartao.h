#ifndef ENTIDADE_CARTAO_H
#define ENTIDADE_CARTAO_H

#include "dominiosCartao.h"

using namespace std;

///
/// %Classe da entidade cartão de crédito.
///
class Cartao {
private:

    NumeroCartao numero;
    CodigoSeguranca codigo;
    DataValidade validade;

public:

    ///
    /// Método setter para o número do cartão.
    ///
    void setNumero(const NumeroCartao &numero) {
        this->numero = numero;
    }

    ///
    /// Método getter para o número do cartão.
    ///
    NumeroCartao getNumero() const {
        return numero;
    }

    ///
    /// Método setter para o codigo de segurança do cartão.
    ///
    void setCodigo(const CodigoSeguranca &codigo) {
        this->codigo = codigo;
    }

    ///
    /// Método getter para o codigo de segurança do cartão.
    ///
    CodigoSeguranca getCodigo() const {
        return codigo;
    }

    ///
    /// Método setter para a data de validade do cartão.
    ///
    void setValidade(const DataValidade &validade) {
        this->validade = validade;
    }

    ///
    /// Método getter para a data de validade do cartão.
    ///
    DataValidade getValidade() const {
        return validade;
    }
};

#endif