#ifndef RESULTADOS_H
#define RESULTADOS_H

#include "Apresentacao.h"
#include "Cartao.h"
#include "Evento.h"
#include "Ingresso.h"
#include "Usuario.h"

// Módulo de resultados para implementação de serviços do sistema.

class Resultado {
protected:
    int valor;

public:

    const static int SUCESSO = 0;
    const static int FALHA   = 1;

    void setValor(int valor){
        this->valor = valor;
    }

    int getValor() const {
        return valor;
    }
};

class ResultadoCadastro:public Resultado {
private:
    CPF cpf;
    Senha senha;

public:
    void setCPF(const CPF &cpf){
        this->cpf = cpf;
    }
    void setSenha(const Senha &senha){
        this->senha = senha;
    }

    CPF getCPF() const {
        return cpf;
    }
    Senha getSenha() const {
        return senha;
    }
};


#endif