# TP1

Esse repositório é destinado ao trabalho da matéria de técnicas de programação 1, que consiste em desenvolver um sistema de venda de ingressos exercitando a modularização e o uso do paradigma de orientação a objeto.

O padrão de codificação adotado para esse projeto foi o seguinte:

- Nomes de classe em _cammel case_ com inicial maiúscula e supressão de conectivos (de, do, etc...). Exemplo:

  > NomeClasse

- Nome de métodos em _cammel case_ com inicial minúscula e supressão de conectivos (de, do, etc...). Exemplo:
  
  > nomeMetodo()

- Nome de variáveis em _snake case_ com letras minúsculas. Exemplo:

  > nome\_da\_variavel

- Nome de constante em _snake case_ com letras maiúsculas. Exemplo:

  > NOME\_DA\_CONSTANTE

- Prefixo TU para testes de unidade.

A estrutura de pastas e o padrão de nomenclatura adotado foi o seguinte:

- Pasta _Entidades_ contendo as pastas das demais entidades da biblioteca;

- Pastas homônimas à sua entidade;

- Dentro de cada pasta de entidade há o seguinte conteúdo:
  - Um arquivo _Entidade.h_
  - Um par de arquivos _dominiosEntidade.cpp_ e _dominiosEntidade.h_
  - Um par de arquivos _testesEntidade.cpp_ e _testesEntidade.h_

- Fora da pasta _Entidades_:
  - Arquivo _TesteDeUnidade.h_, que contém a classe abstrata TU herdada pelos demais testes de unidade;
  - Arquivo _test_main.cpp_, que contém a função que roda todos os testes definidos e mostra seus resultados no terminal.